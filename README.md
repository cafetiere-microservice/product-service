# Cafetiere Application

## Status
[![coverage report](https://gitlab.com/cafetiere-microservice/product-service/badges/master/coverage.svg)](https://gitlab.com/cafetiere-microservice/product-service/-/commits/master/)
[![pipeline status](https://gitlab.com/cafetiere-microservice/product-service/badges/master/pipeline.svg)](https://gitlab.com/cafetiere-microservice/product-service/-/commits/master/)

## Group Member
Ghifari Aulia Azhar Riza

## Feature

- CRUD Product (admin)

## Production Notes
- [Database Schema](http://bit.ly/cafetiereSchema)
- [Deployed Website](https://cafetiere-product-service.herokuapp.com)

## Project Structure
- **Controller**: put all your endpoint controller here.
- **Model**: Entity class for your database.
- **Repository**: Creating database query.
- **Service**: Business logic code is here.
