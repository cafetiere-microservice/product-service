package com.cafetiere.product.service;

import com.cafetiere.product.model.Product;
import com.cafetiere.product.repository.ProductRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProductServiceImplTest {
    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private ProductServiceImpl productService;

    private Product product;

    @BeforeEach
    public void setUp() {
        product = new Product();
        product.setId(1);
        product.setName("Latte");
        product.setDescription("ini latte");
        product.setPrice(17000);
        product.setQuantity(1);
    }

    @Test
    void testServiceCreateProduct() {
        when(productRepository.save(product)).thenReturn(product);
        Product created = productService.createProduct(product);
        assertEquals(created.getName(), product.getName());
        verify(productRepository).save(product);
    }

    @Test
    void testServiceGetListProduct() {
        Iterable<Product> listProduct = productRepository.findAll();
        lenient().when(productService.getListProduct()).thenReturn(listProduct);
        Iterable<Product> listProductResult = productService.getListProduct();
        Assertions.assertIterableEquals(listProduct, listProductResult);
    }

    @Test
    void testServiceGetProductById() {
        lenient().when(productService.getProductById(1)).thenReturn(product);
        when(productRepository.findById(1)).thenReturn(product);
        Product productResult = productService.getProductById(product.getId());
        assertEquals(product.getId(), productResult.getId());
    }

    @Test
    void testServiceDeleteProductById() {
        productService.createProduct(product);
        when(productRepository.findById(1)).thenReturn(product);
        productService.deleteProductById(1);
        lenient().when(productService.getProductById(1)).thenReturn(null);
        assertNull(productService.getProductById(product.getId()));
        verify(productRepository).deleteById(1);
    }

    @Test
    void testServiceUpdateProduct() {
        productService.createProduct(product);
        String currentName = product.getName();
        String productName = "Americano";
        product.setName(productName);
        Product expectedProduct = product;
        when(productRepository.findById(product.getId())).thenReturn(product);
        assertNotEquals(expectedProduct.getName(), currentName);
        Product productResult = productService.updateProduct(product.getId(), product);
        assertEquals(expectedProduct.getName(), productResult.getName());
    }

    @Test
    void testNonExistServiceUpdateProduct() {
        Product productResult = productService.updateProduct(product.getId(), product);
        assertNull(productResult);
    }
}

